#!/usr/bin/env python3

from pattern.text.en import singularize, pluralize
import plotly.express as px
import argparse


# Global variables
INPUT_TEXT = ">> "


# Project functions
def parsing_arguments():
    parser = argparse.ArgumentParser(description="Draw chart from custom given informations")
    args = parser.parse_args()

    return args


def draw_chart(data):
    fig = px.line(data, x="Quantity", y="Utility")
    fig.update_layout(
        title_text="Quantity and Utility (" + data["product_plural"].capitalize() + ')'
    )
    fig.show()


def main():
    args = parsing_arguments()

    data = {
        "product_singular": "",
        "product_plural": "",
        "quantity_info": 0,
        "Utility": [],
        "Quantity": []
    }

    print("\n>> Please enter a product")
    data["product_plural"] = pluralize(input(INPUT_TEXT).lower())
    data["product_singular"] = singularize(data["product_plural"])
    print("Please enter the maximum quantity")
    data["quantity_info"] = int(input(INPUT_TEXT))
    print("\n")

    for index in range(data["quantity_info"]):
        if index + 1 == 1:
            print(">> For " + str(index + 1) + " " + data["product_singular"] + " what added happiness do you get?")
        else:
            print(">> For " + str(index + 1) + " " + data["product_plural"] + " what added happiness do you get?")
        data["Utility"].append(input(INPUT_TEXT))
        data["Quantity"].append(index + 1)

    draw_chart(data)

if __name__ == "__main__":
    main()